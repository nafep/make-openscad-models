// CSG-modules.scad - Basic usage of modules, if, color, $fs/$fa

include <BOLTS/BOLTS.scad>

$fn=50;

mountscrewdist = 30;
thickness = 18;
thickness2 = 10;
height = 18;
height2 = 18;

width = mountscrewdist + 2*6;

dia = get_dim(ISO4017_dims("M3"),"d1");
hdia = get_dim(ISO4017_dims("M3"),"e");
hthickness = get_dim(ISO4017_dims("M3"),"k");

module screw_hole() {
    translate ([0,0,-0.01])
    scale(1.02)
    union() {
        translate([0,0,0]) cylinder(h=6,d=5.5,center=true);
        translate ([0,0,hthickness]) {
            //rotate([180,0,0])
            ISO4762("M3",l=thickness+2);
        }
    }
}

module bottom_screw_hole() {
    scale(1.01)
        translate ([0,0,hthickness])
            ISO4017("M3");
}


module base_plate() {
    difference() {
      cube([58,51,thickness]);

      translate([0,11,-1]) {
        rotate([0,0,225]) 
            cube([50,50,thickness+2]);
        translate([58,0,0]) 
        rotate([0,0,225]) 
            cube([50,50,thickness+2]);  
      }
                
//      translate([14,51-16,0]) bottom_screw_hole();
//      translate([44,51-16,0]) bottom_screw_hole();    
    }
}



module jhead_top() {
    scale([1.02,1.02,1])
    union() {
        cylinder (d=16,h=3.2);
        translate ([0,0,13-4.2]) cylinder (d=16,h=4.2);
        cylinder (d=12,h=13);
    }
}



module jhead_mount() {
    difference() {
        union() {
        translate ([0,thickness-thickness2/2,height/2]) 
            cube([width,thickness2,height],center=true);
        translate ([0,thickness/2,height2/2]) 
            cube([width,thickness,height2],center=true);
        }
        
        translate([-mountscrewdist/2,0,height-5])
            rotate([-90,0,0])
            screw_hole();
        translate([mountscrewdist/2,0,height-5])
            rotate([-90,0,0])
            screw_hole();
       
        y=thickness-5;
        
        translate([0,-thickness/2+y+0.5,6]) {
            cube([36,thickness,6.5],center=true);
        }
        
        translate([-10.5,y-7,height+1]) {
            union(){ translate([0,0,-1]) cylinder(h=1,d=4,center=true);
            translate([0,0,-get_dim(ISO4762_dims("M3"),"k")])
            rotate([180,0,0])
            ISO4762("M4",l=thickness/2);
            }
        }
        
        translate([10.5,y-7,height+1]) {
            union(){ translate([0,0,-1]) cylinder(h=1,d=4,center=true);
            translate([0,0,-get_dim(ISO4762_dims("M3"),"k")])
                rotate([180,0,0])
                    ISO4762("M4",l=thickness/2);
            }
        }
        
        translate([0,thickness-8-5.45-5,0]) 
        union() {
            rotate([-90,0,180]) 
                linear_extrude(height=thickness/2) 
                    projection() 
                        rotate ([90,0,0]) 
                            jhead_top();
            jhead_top();
            translate([0,0,10])
                cylinder(h=10,d=12);
        }
    }
}


difference() {
    jhead_mount();
}

//translate ([0,thickness-(8+5.5+5),0]) jhead_top();