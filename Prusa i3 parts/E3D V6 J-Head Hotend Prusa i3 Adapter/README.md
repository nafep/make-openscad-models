###E3D V6 J-Head Hotend 

![](http://i.ebayimg.com/images/i/181962444807-0-0/s-l140/p.jpg)

*Bought the hotend with the MK8 extruder on ebay.be in 2015.*

The present OpenSCAD piece adapts this hotend model to the two screw mount of the Prusa i3.

**Dependencies**: BOLTS library