# Z Axis top support

Original file name: ``` EjeZ_soporte_superior.stl ```

I have adapted it to allow fine tuning position of the z-axis along the x-axis. 

Assuming the use of a board (e.g. MDF) for building the frame, I consider the y-axis to be well aligned. However, as the alignment of the drill holes for the Z Axis top support may lack sufficient accuracy (as it did for my first build), being able to easily adjust the x-position of the support could (partially) solve the issue. 

Special attention has to be taken to make the adjustment resistant to vibrations.